package com.bimpos.secret.santa.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.adapters.GroupsAdapter;
import com.bimpos.secret.santa.databinding.FragmentHomeBinding;
import com.bimpos.secret.santa.models.Groups;
import com.bimpos.secret.santa.models.User;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.List;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private MainActivity mainActivity;
    private User user;
    private static final String TAG = "HomeFragment";

    public HomeFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        initVariables();
        fillContent();
        initClickListeners();
        checkGroups();

        return binding.getRoot();
    }

    private void fillContent() {
        user = FastSave.getInstance().getObject("user", User.class);
        binding.userName.setText(user.getName());
        binding.email.setText(user.getEmail());

    }

    private void initClickListeners() {
        binding.createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.openCreateFragment();

            }
        });

        binding.joinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openJoinGroupFragment();
            }
        });
    }

    private void openJoinGroupFragment() {
        mainActivity.openJoinGroupFragment();
    }

    public void checkGroups() {
        Log.d(TAG, "checkGroups: start");
        List<Groups> groupsList = FastSave.getInstance().getObjectsList("groups", Groups.class);
        if (groupsList == null) {
            Log.d(TAG, "checkGroups: null");
        }
        GroupsAdapter adapter = new GroupsAdapter(getContext(), mainActivity, groupsList);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerView.setAdapter(adapter);

    }

    private void initVariables() {
        new ClickShrinkEffect(binding.createGroup);
        new ClickShrinkEffect(binding.joinGroup);
    }
}