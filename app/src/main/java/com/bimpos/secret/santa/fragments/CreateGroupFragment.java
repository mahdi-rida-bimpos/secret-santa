package com.bimpos.secret.santa.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.databinding.FragmentCreateGroupBinding;
import com.bimpos.secret.santa.helpers.CustomLoading;
import com.bimpos.secret.santa.models.Groups;
import com.bimpos.secret.santa.models.User;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateGroupFragment extends Fragment {

    private FragmentCreateGroupBinding binding;
    private MainActivity mainActivity;
    private Groups groups;
    private static final String TAG = "CreateGroupFragment";
    private CustomLoading loading;
    private final Calendar calendar = Calendar.getInstance();



    public CreateGroupFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCreateGroupBinding.inflate(getLayoutInflater());
        initVariables();
        initClickListeners();

        return binding.getRoot();
    }

    private void initVariables() {
        loading = new CustomLoading(getActivity());
        new ClickShrinkEffect(binding.createGroup);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainActivity.loadRewardedAd();
    }

    private void initClickListeners() {
        binding.createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGroup();
            }
        });
    }

    private void createGroup() {
        String groupName = binding.groupName.getText().toString();
        String budget = binding.budget.getText().toString();

        if (TextUtils.isEmpty(groupName) || TextUtils.isEmpty(budget)) {
            Toast.makeText(getActivity(), "Please fill all fields above", Toast.LENGTH_SHORT).show();
        } else {
            loading.show();
            loading.startAnimation(R.raw.loading);
            List<Groups> groupsList = FastSave.getInstance().getObjectsList("groups", Groups.class);
            if (groupsList == null) {
                groupsList = new ArrayList<>();
            }
            List<User> userList = new ArrayList<>();
            User user = FastSave.getInstance().getObject("user", User.class);
            userList.add(user);
            groupsList.add(new Groups(groupName, Integer.parseInt(budget), "12345", userList));
            FastSave.getInstance().deleteValue("groups");
            FastSave.getInstance().saveObjectsList("groups", groupsList);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainActivity.onBackPressed();
                    loading.dismiss();
                }
            }, 1000);
        }

    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), dateListener, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    DatePickerDialog.OnDateSetListener dateListener = (view, year, monthOfYear, dayOfMonth) -> {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        String date = format.format(calendar.getTime());
    };
}