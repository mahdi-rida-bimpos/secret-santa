package com.bimpos.secret.santa.models;


public class User {

    private String name;
    private String email;
    private String password;
    private int isRaffled;
    private int isValidated;

    public User() {
    }


    public int getIsValidated() {
        return isValidated;
    }

    public void setIsValidated(int isValidated) {
        this.isValidated = isValidated;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsRaffled() {
        return isRaffled;
    }

    public void setIsRaffled(int isRaffled) {
        this.isRaffled = isRaffled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
