package com.bimpos.secret.santa.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.models.Groups;
import com.bimpos.secret.santa.models.User;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private List<User> userList;
    private int groupPosition;
    private Groups groups;
    private User currentUSer;
    private static final String TAG = "GroupsAdapter";

    public UsersAdapter(int groupPosition) {
        this.groupPosition = groupPosition;
        groups=FastSave.getInstance().getObjectsList("groups",Groups.class).get(groupPosition);
        currentUSer = FastSave.getInstance().getObject("user",User.class);
        this.userList = groups.getParticipantsList();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_users, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (userList != null && userList.size() != 0) {

            holder.pendingLayout.setVisibility(View.GONE);
            holder.mainLayout.setVisibility(View.VISIBLE);
            User user = userList.get(position);

            holder.userName.setText(user.getName());
            holder.userEmail.setText(user.getEmail());

            if(user.getName().equalsIgnoreCase(currentUSer.getName())){
                holder.delete.setVisibility(View.GONE);
                holder.userName.setText(user.getName()+" (You) ");
            }

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    userList.remove(user);
                    groups.setParticipantsList(userList);
                   List<Groups> groupsList = FastSave.getInstance().getObjectsList("groups", Groups.class);
                    for (int i = 0; i < groupsList.size(); i++) {
                        Groups groups1 = groupsList.get(i);
                        if (groups1.getGroupName().equalsIgnoreCase(groups.getGroupName())) {
                            Log.d(TAG, "onClick: got it");
                            groupsList.remove(groups1);
                            groupsList.add(groups);
                            break;
                        }
                    }
                    FastSave.getInstance().deleteValue("groups");
                    FastSave.getInstance().saveObjectsList("groups", groupsList);
                    notifyItemRemoved(holder.getAdapterPosition());
                }
            });


        } else {
            holder.pendingLayout.setVisibility(View.VISIBLE);
            holder.mainLayout.setVisibility(View.GONE);
        }
    }

    public void setData(List<User> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        if (userList != null && userList.size() != 0) {
            return userList.size();
        } else {
            return 1;
        }
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userEmail;
        ImageView delete;
        RelativeLayout mainLayout, pendingLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.recycler_users_name);
            userEmail = itemView.findViewById(R.id.recycler_users_email);
            delete = itemView.findViewById(R.id.recycler_user_delete);
            mainLayout = itemView.findViewById(R.id.recycler_users_mainLayout);
            pendingLayout = itemView.findViewById(R.id.recycler_users_pendingLayout);

        }
    }
}
