package com.bimpos.secret.santa.models;


import java.util.ArrayList;
import java.util.List;

public class Groups {

    private String groupName;
    private int budget;
    private String groupCode;
    private List<User> participantsList;

    public Groups(String groupName, int budget,String groupCode,List<User> userList) {
        this.budget = budget;
        this.groupCode = groupCode;
        this.groupName = groupName;
        this.participantsList = userList;
    }

    public Groups() {
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public List<User> getParticipantsList() {
        return participantsList;
    }

    public void setParticipantsList(List<User> participantsList) {
        this.participantsList = participantsList;
    }
}
