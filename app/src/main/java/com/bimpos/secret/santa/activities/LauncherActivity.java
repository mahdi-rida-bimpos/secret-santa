package com.bimpos.secret.santa.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.bimpos.secret.santa.BuildConfig;
import com.bimpos.secret.santa.databinding.ActivityLauncherBinding;

public class LauncherActivity extends AppCompatActivity {

    private ActivityLauncherBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLauncherBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        new Handler().postDelayed(this::gotoMainActivity,2000);
    }

    private void initVariables() {
        String versionName = "version "+ BuildConfig.VERSION_NAME;
        binding.version.setText(versionName);
    }

    private void gotoMainActivity() {
        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}