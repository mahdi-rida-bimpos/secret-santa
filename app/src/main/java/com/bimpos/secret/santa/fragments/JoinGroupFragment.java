package com.bimpos.secret.santa.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.databinding.FragmentJoinGroupBinding;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

public class JoinGroupFragment extends Fragment {

    private FragmentJoinGroupBinding binding;
    private MainActivity activity;

    public JoinGroupFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentJoinGroupBinding.inflate(getLayoutInflater());

        new ClickShrinkEffect(binding.join);

        initClickListeners();

        return binding.getRoot();
    }

    private void initClickListeners() {
        binding.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCodeEmpty()) {
                    //todo checkGroup
                } else {
                    Toast.makeText(getActivity(), "Please write your invitation code", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activity.loadRewardedAd();
    }

    private boolean isCodeEmpty() {
        String code = binding.code.getText().toString();
        if (TextUtils.isEmpty(code)) {
            return true;
        }
        return false;
    }
}