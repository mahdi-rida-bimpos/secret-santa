package com.bimpos.secret.santa.application;

import android.app.Application;

import com.appizona.yehiahd.fastsave.FastSave;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FastSave.init(getApplicationContext());
    }
}
