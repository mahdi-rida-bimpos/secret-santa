package com.bimpos.secret.santa.adapters;

import android.content.Context;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.models.Groups;

import java.util.List;

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder> {

    private Context context;
    private MainActivity activity;
    private List<Groups> groupsList;
    private static final String TAG = "GroupsAdapter";

    public GroupsAdapter(Context context, MainActivity activity, List<Groups> groupsList) {
        this.context = context;
        this.activity = activity;
        this.groupsList = groupsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_groups, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (groupsList != null && groupsList.size() != 0) {
            holder.pendingLayout.setVisibility(View.GONE);
            holder.mainLayout.setVisibility(View.VISIBLE);
            Groups groups = groupsList.get(position);
            holder.groupName.setText(groups.getGroupName());
            int participantNumbers = groups.getParticipantsList().size();
            if (participantNumbers == 0) {
                holder.groupStatus.setText("No Participants");
            } else if (participantNumbers <= 1) {
                holder.groupStatus.setText("Pending Participants");
            } else {
                holder.groupStatus.setText("Sufficient Participants");
                holder.groupStatus.setTextColor(ContextCompat.getColor(context,R.color.green));
            }
            holder.participantsNb.setText(String.valueOf(participantNumbers));

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.openGroup(holder.getAdapterPosition());
                }
            });

        } else {
            holder.pendingLayout.setVisibility(View.VISIBLE);
            holder.mainLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {

        if (groupsList != null && groupsList.size() != 0) {
            return groupsList.size();
        } else {
            return 1;
        }
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView groupName, participantsNb, groupStatus;
        RelativeLayout mainLayout, pendingLayout;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            groupStatus = itemView.findViewById(R.id.recycler_groups_status);
            participantsNb = itemView.findViewById(R.id.recycler_groups_participants_nb);
            groupName = itemView.findViewById(R.id.recycler_groups_name);
            mainLayout = itemView.findViewById(R.id.recycler_groups_mainLayout);
            pendingLayout = itemView.findViewById(R.id.recycler_groups_pendingLayout);
            cardView = itemView.findViewById(R.id.recycler_groups_card);
        }
    }
}
