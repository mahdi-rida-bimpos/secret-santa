package com.bimpos.secret.santa.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.adapters.UsersAdapter;
import com.bimpos.secret.santa.databinding.FragmentGroupDetailBinding;
import com.bimpos.secret.santa.helpers.CustomDialog;
import com.bimpos.secret.santa.helpers.CustomLoading;
import com.bimpos.secret.santa.models.Groups;
import com.bimpos.secret.santa.models.User;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.List;

public class GroupDetailFragment extends Fragment implements CustomDialog.DialogEvents {

    private FragmentGroupDetailBinding binding;
    private MainActivity activity;
    private final Groups groups;
    private UsersAdapter adapter;
    private final int groupPosition;
    private CustomDialog dialog;
    private User user;
    private final int DIALOG_MESSAGE = 1;
    private final int DIALOG_ADD = 2;
    private final int DIALOG_PASSWORD = 3;
    private final int DIALOG_WINNER = 4;
    private final int DIALOG_LEAVE = 5;
    private final int DIALOG_DELETE = 6;

    private CustomLoading loading;
    private static final String TAG = "GroupDetailFragment";

    public GroupDetailFragment(MainActivity activity, int groupPosition) {
        this.activity = activity;
        this.groupPosition = groupPosition;
        groups = FastSave.getInstance().getObjectsList("groups", Groups.class).get(groupPosition);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        initVariables();
        initRecyclerView();
        initClickListeners();

        return binding.getRoot();
    }

    private void initVariables() {
        binding = FragmentGroupDetailBinding.inflate(getLayoutInflater());

        user = FastSave.getInstance().getObject("user", User.class);
        handleBottomLayout();

        loading = new CustomLoading(getActivity());

        new ClickShrinkEffect(binding.raffle);

        binding.groupName.setText(groups.getGroupName());
        binding.groupCode.setText(groups.getGroupCode());
        dialog = new CustomDialog(GroupDetailFragment.this);
    }

    private void handleBottomLayout() {
        if (user.getIsRaffled() == 1) {
            binding.raffle.setEnabled(false);
            binding.raffle.setClickable(false);
            binding.giftLayout.setVisibility(View.VISIBLE);
            binding.raffle.setVisibility(View.INVISIBLE);
        } else {
            binding.raffle.setEnabled(true);
            binding.raffle.setClickable(true);
            binding.raffle.setVisibility(View.VISIBLE);
            binding.giftLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void initClickListeners() {
        binding.giftLayout.setOnClickListener(v -> showPasswordDialog());

        binding.addParticipants.setOnClickListener(v -> showAddUserDialog());

        binding.raffle.setOnClickListener(v -> {
            List<User> participants = groups.getParticipantsList();
            if (participants.size() < 2) {
                showSimpleDialog("Attention", "Must join 3 participants to raffle", "Ok", DIALOG_MESSAGE, CustomDialog.DIALOG_TYPE_MESSAGE);
            } else {
                startRaffling();
            }
        });

        binding.share.setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, groups.getGroupCode());
            sendIntent.setType("text/plain");
            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);
        });

        binding.leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSimpleDialog("Attention", "Are you sure?", "Yes", DIALOG_LEAVE, CustomDialog.DIALOG_TYPE_MESSAGE);
            }
        });

        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSimpleDialog("Attention", "Are you sure?", "Yes", DIALOG_DELETE, CustomDialog.DIALOG_TYPE_MESSAGE);
            }
        });

    }

    private void startRaffling() {
        loading.show();
        loading.startAnimation(R.raw.raffle1);
        new Handler().postDelayed(() -> {
            showWinnerDialog();
            user.setIsRaffled(1);
            FastSave.getInstance().deleteValue("user");
            FastSave.getInstance().saveObject("user", user);
        }, 3000);
    }

    private void showPasswordDialog() {
        showSimpleDialog("Your Password", "", "Ok", DIALOG_PASSWORD, CustomDialog.DIALOG_TYPE_REVEAL_PASSWORD);
    }

    private void showWinnerDialog() {
        if (loading.isShowing()) {
            loading.dismiss();
        }
        new Handler().postDelayed(() -> {
            showSimpleDialog("Surprise!", "The person you have to gift is\nDany", "Accept", DIALOG_WINNER, CustomDialog.DIALOG_TYPE_MESSAGE);
        }, 200);

    }

    private void showSimpleDialog(String title, String message, String positiveButton, int dialogId, int dialogType) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, title);
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, positiveButton);
        arguments.putInt(CustomDialog.DIALOG_ID, dialogId);
        arguments.putInt("type", dialogType);
        dialog.setArguments(arguments);
        dialog.show(requireActivity().getSupportFragmentManager(), null);
        dialog.setCancelable(true);
    }

    private void showAddUserDialog() {
        showSimpleDialog("User Info", "", "submit", DIALOG_ADD, CustomDialog.DIALOG_TYPE_ADD_USER);
    }

    private void initRecyclerView() {
        adapter = new UsersAdapter(groupPosition);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
    }


    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        dialog.dismiss();

        switch (dialogId) {
            case DIALOG_ADD:
                User user = new User();
                if (args.containsKey("username")) {
                    user.setName(args.getString("username"));
                }
                if (args.containsKey("useremail")) {
                    user.setEmail(args.getString("useremail"));
                }
                user.setIsRaffled(0);
                List<User> userList = groups.getParticipantsList();
                userList.add(user);

                groups.setParticipantsList(userList);
                List<Groups> groupsList = FastSave.getInstance().getObjectsList("groups", Groups.class);
                for (int i = 0; i < groupsList.size(); i++) {
                    Groups groups1 = groupsList.get(i);
                    if (groups1.getGroupCode().equalsIgnoreCase(groups.getGroupCode())) {
                        Log.d(TAG, "onClick: got it");
                        groupsList.remove(groups1);
                        groupsList.add(groups);
                        break;
                    }
                }

                FastSave.getInstance().deleteValue("groups");
                FastSave.getInstance().saveObjectsList("groups", groupsList);
                adapter.setData(groups.getParticipantsList());

                break;

            case DIALOG_MESSAGE:
                dialog.dismiss();
                break;

            case DIALOG_WINNER:
                handleBottomLayout();
                dialog.dismiss();
                break;

            case DIALOG_PASSWORD:
                dialog.dismiss();
                if (args.containsKey("password")) {
                    String password = args.getString("password");
                    User user1 = FastSave.getInstance().getObject("user", User.class);
                    if (password.equalsIgnoreCase(user1.getPassword())) {
                        new Handler().postDelayed(() -> showSimpleDialog("Shush!", "The person you have to gift is\nDany", "Got it", DIALOG_MESSAGE, CustomDialog.DIALOG_TYPE_MESSAGE), 200);
                    } else {
                        new Handler().postDelayed(() -> showSimpleDialog("Sorry", "Password incorrect", "Ok", DIALOG_MESSAGE, CustomDialog.DIALOG_TYPE_MESSAGE), 200);
                    }
                }

            case DIALOG_LEAVE:
                dialog.dismiss();
                //todo leave
                break;

            case DIALOG_DELETE:
                dialog.dismiss();
                //todo leave
                break;
        }

    }
}