package com.bimpos.secret.santa.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.databinding.FragmentInfoBinding;
import com.bimpos.secret.santa.models.User;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InfoFragment extends Fragment {

    private FragmentInfoBinding binding;
    private MainActivity mainActivity;

    public InfoFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentInfoBinding.inflate(getLayoutInflater());
        new ClickShrinkEffect(binding.next);
        checkUser();
        binding.next.setOnClickListener(v -> login());

        return binding.getRoot();
    }

    private void checkUser() {
        User user = FastSave.getInstance().getObject("user",User.class);
        if(user!=null){
            binding.name.setText(user.getName());
            binding.email.setText(user.getEmail());
            binding.password.setText(user.getPassword());
        }
    }

    private void login() {
        String userName = binding.name.getText().toString();
        String email = binding.email.getText().toString();
        String password = binding.password.getText().toString();

        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            if(isEmailValid(email)){
                User user = new User();
                user.setName(userName);
                user.setEmail(email);
                user.setPassword(password);
                user.setIsValidated(0);
                FastSave.getInstance().saveObject("user",user);
                mainActivity.validateUser();
            }else{
                Toast.makeText(getActivity(), "Write your email correctly", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getActivity(), "Please fill all fields above", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}