package com.bimpos.secret.santa.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.R;
import com.bimpos.secret.santa.databinding.ActivityMainBinding;
import com.bimpos.secret.santa.fragments.CodeValidationFragment;
import com.bimpos.secret.santa.fragments.CreateGroupFragment;
import com.bimpos.secret.santa.fragments.GroupDetailFragment;
import com.bimpos.secret.santa.fragments.HomeFragment;
import com.bimpos.secret.santa.fragments.InfoFragment;
import com.bimpos.secret.santa.fragments.JoinGroupFragment;
import com.bimpos.secret.santa.helpers.CustomDialog;
import com.bimpos.secret.santa.helpers.CustomLoading;
import com.bimpos.secret.santa.models.User;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

public class MainActivity extends AppCompatActivity implements CustomDialog.DialogEvents {

    private ActivityMainBinding binding;
    private CustomLoading loading;
    private User user;
    private CustomDialog dialog;
    private final int DIALOG_MESSAGE = 1;
    private static final String TAG = "MainActivity";

    private AdRequest adRequest;
    private RewardedAd mRewardedAd;
    private static final String AD_UNIT_ID = "/6499/example/rewarded-video";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initVariables();
        initClickListeners();
        checkUser();
        loadRewardedAd();

    }

    public void loadRewardedAd() {
        if (mRewardedAd == null) {
//            AdManagerAdRequest adRequest = new AdManagerAdRequest.Builder().build();
            AdRequest adRequest = new AdRequest.Builder().build();
            RewardedAd.load(
                    this,
                    AD_UNIT_ID,
                    adRequest,
                    new RewardedAdLoadCallback() {
                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            // Handle the error.
                            mRewardedAd = null;
                            loadRewardedAd();
                            Log.d(TAG, "onAdFailedToLoad: failed " + loadAdError.getMessage());
                        }

                        @Override
                        public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                            mRewardedAd = rewardedAd;
                            Log.d(TAG, "onAdLoaded");
                        }
                    });
        }
    }

    private void showRewardedVideo() {
        if (mRewardedAd == null) {
            Log.d(TAG, "The rewarded ad wasn't ready yet.");

            return;
        }

        mRewardedAd.setFullScreenContentCallback(
                new FullScreenContentCallback() {
                    @Override
                    public void onAdShowedFullScreenContent() {
                        // Called when ad is shown.
                        Log.d(TAG, "onAdShowedFullScreenContent");
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(AdError adError) {
                        // Called when ad fails to show.
                        Log.d(TAG, "onAdFailedToShowFullScreenContent");
                        mRewardedAd = null;
                    }

                    @Override
                    public void onAdDismissedFullScreenContent() {
                        Log.d(TAG, "onAdDismissedFullScreenContent");
                        mRewardedAd = null;
                        // Preload the next rewarded ad.
                        loadRewardedAd();
                    }
                });
        Activity activityContext = this;
        mRewardedAd.show(
                activityContext,
                new OnUserEarnedRewardListener() {
                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                        // Handle the reward.
                        Log.d("TAG", "The user earned the reward.");
                        int rewardAmount = rewardItem.getAmount();
                        String rewardType = rewardItem.getType();
                    }
                });
    }


    private void initVariables() {
        dialog = new CustomDialog(this);
        loading = new CustomLoading(this);
        binding.santaClaus.animate().scaleX(1).scaleY(1).setDuration(500).setStartDelay(500).start();
    }

    private void showAlertDialog(String title, String message, int dialogId) {
        Bundle arguments = new Bundle();
        arguments.putString(CustomDialog.DIALOG_MESSAGE, message);
        arguments.putString(CustomDialog.DIALOG_TITLE, title);
        arguments.putString(CustomDialog.DIALOG_POSITIVE_BUTTON, "Validate");
        arguments.putInt(CustomDialog.DIALOG_ID, dialogId);
        arguments.putInt("type", CustomDialog.DIALOG_TYPE_MESSAGE);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), null);
        dialog.setCancelable(true);
    }

    private void checkUser() {
        User user = FastSave.getInstance().getObject("user", User.class);
        if (user != null) {
            openHomeFragment();
        } else {
            openInfoFragment();
        }
    }


    public void openHomeFragment() {
        binding.back.setVisibility(View.GONE);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, new HomeFragment(this), "home")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void openInfoFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, new InfoFragment(this))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void openGroup(int position) {
        binding.back.setVisibility(View.VISIBLE);
        GroupDetailFragment fragment = new GroupDetailFragment(this, position);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void openCreateFragment() {
        user = FastSave.getInstance().getObject("user", User.class);
        if (user.getIsValidated() == 1) {
            binding.back.setVisibility(View.VISIBLE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameLayout, new CreateGroupFragment(this))
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
            showRewardedVideo();
        } else {
            showAlertDialog("Attention", "You have to validate your account first", DIALOG_MESSAGE);
        }
    }

    private void openValidationFragment() {
        binding.back.setVisibility(View.VISIBLE);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, new CodeValidationFragment(this), "validation")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void openJoinGroupFragment() {
        binding.back.setVisibility(View.VISIBLE);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, new JoinGroupFragment(this), "join")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        showRewardedVideo();
    }

    private void initClickListeners() {
        binding.back.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        binding.back.setVisibility(View.GONE);
        if (homeFragmentExists()) {
            super.onBackPressed();
        } else if (validationFragmentExists()) {
            openInfoFragment();
        } else {
            openHomeFragment();
        }

    }

    private boolean validationFragmentExists() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("validation");
        if (fragment == null) {
            return false;
        }
        return true;
    }

    private boolean homeFragmentExists() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("home");
        if (fragment == null) {
            return false;
        }
        return true;
    }

    public void gotUser() {
        loading.show();
        loading.startAnimation(R.raw.loading);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loading.dismiss();
                openHomeFragment();
            }
        }, 1000);
    }

    public void validateUser() {
        openValidationFragment();
    }

    @Override
    public void onPositiveDialogResult(int dialogId, Bundle args) {
        switch (dialogId) {
            case DIALOG_MESSAGE:
                dialog.dismiss();
                openValidationFragment();
        }
    }
}