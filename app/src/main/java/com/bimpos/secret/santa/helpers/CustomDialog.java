package com.bimpos.secret.santa.helpers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bimpos.secret.santa.R;


public class CustomDialog extends AppCompatDialogFragment {


    public static final String DIALOG_ID = "id";
    public static final String DIALOG_MESSAGE = "message";
    public static final String DIALOG_TITLE = "title";
    public static final String DIALOG_POSITIVE_BUTTON = "positiveButton";
    private final DialogEvents listener;
    public static final int DIALOG_TYPE_ADD_USER = 1;
    public static final int DIALOG_TYPE_MESSAGE = 2;
    public static final int DIALOG_TYPE_REVEAL_PASSWORD = 3;
    private int dialogType;

    public interface DialogEvents {
        void onPositiveDialogResult(int dialogId, Bundle args);
    }

    public CustomDialog(DialogEvents listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        @SuppressLint("InflateParams")
        View dialogView = getLayoutInflater().inflate(R.layout.custom_dialog, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);

        final Bundle arguments = getArguments();
        final int dialogId;
        String messageString;
        String titleText;
        String positiveButtonText;

        TextView title = dialogView.findViewById(R.id.custom_dialog_title);
        TextView message = dialogView.findViewById(R.id.custom_dialog_message);
        TextView positiveButton = dialogView.findViewById(R.id.custom_dialog_button);
        EditText userName = dialogView.findViewById(R.id.custom_dialog_userName);
        EditText userEmail = dialogView.findViewById(R.id.custom_dialog_userEmail);
        EditText password = dialogView.findViewById(R.id.custom_dialog_password);
        LinearLayout inputLayout = dialogView.findViewById(R.id.custom_dialog_inputLayout);


        if (arguments != null) {
            dialogId = arguments.getInt(DIALOG_ID);
            messageString = arguments.getString(DIALOG_MESSAGE);
            positiveButtonText = arguments.getString(DIALOG_POSITIVE_BUTTON);
            titleText = arguments.getString(DIALOG_TITLE);
            dialogType = arguments.getInt("type");
            switch (dialogType) {
                case DIALOG_TYPE_ADD_USER:
                    inputLayout.setVisibility(View.VISIBLE);
                    message.setVisibility(View.GONE);
                    password.setVisibility(View.GONE);
                    break;

                case DIALOG_TYPE_MESSAGE:
                    inputLayout.setVisibility(View.GONE);
                    message.setVisibility(View.VISIBLE);
                    password.setVisibility(View.GONE);
                    break;

                case DIALOG_TYPE_REVEAL_PASSWORD:
                    inputLayout.setVisibility(View.GONE);
                    message.setVisibility(View.GONE);
                    password.setVisibility(View.VISIBLE);
                    break;
            }

        } else {
            throw new IllegalArgumentException("Must pass Dialog_id and Dialog_message in the bundle");
        }

        message.setText(messageString);
        title.setText(titleText);

        positiveButton.setText(positiveButtonText);
        positiveButton.setOnClickListener(v -> {
            if (listener != null) {
                switch (dialogType) {
                    case DIALOG_TYPE_MESSAGE:
                        listener.onPositiveDialogResult(dialogId, arguments);
                        break;

                    case DIALOG_TYPE_ADD_USER:
                        if (userName.getText().toString().length() > 0 && userEmail.getText().toString().length() > 0) {
                            arguments.putString("username", userName.getText().toString());
                            arguments.putString("useremail", userEmail.getText().toString());
                            listener.onPositiveDialogResult(dialogId, arguments);
                        } else {
                            Toast.makeText(getActivity(), "Fields required", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case DIALOG_TYPE_REVEAL_PASSWORD:
                        if (password.getText().toString().length() > 0) {
                            arguments.putString("password", password.getText().toString());
                            listener.onPositiveDialogResult(dialogId, arguments);
                        } else {
                            Toast.makeText(getActivity(), "Fields required", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        });

        return builder.create();
    }
}
