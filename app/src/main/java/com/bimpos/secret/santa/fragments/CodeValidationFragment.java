package com.bimpos.secret.santa.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.appizona.yehiahd.fastsave.FastSave;
import com.bimpos.secret.santa.activities.MainActivity;
import com.bimpos.secret.santa.databinding.FragmentCodeValidationBinding;
import com.bimpos.secret.santa.models.User;
import com.hardik.clickshrinkeffect.ClickShrinkEffect;


public class CodeValidationFragment extends Fragment {

    private FragmentCodeValidationBinding binding;
    private MainActivity activity;

    public CodeValidationFragment(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCodeValidationBinding.inflate(getLayoutInflater());

        initVariables();

        initClickListeners();

        return binding.getRoot();
    }

    private void initVariables() {
        new ClickShrinkEffect(binding.guest);
        new ClickShrinkEffect(binding.validate);
    }

    private void initClickListeners() {
        binding.validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCodeEmpty()) {
                    if (isCodeValid()) {
                        User user = FastSave.getInstance().getObject("user", User.class);
                        user.setIsValidated(1);
                        FastSave.getInstance().deleteValue("user");
                        FastSave.getInstance().saveObject("user", user);
                        activity.openHomeFragment();
                    } else {
                        Toast.makeText(getActivity(), "Invalid Code", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Enter your code please", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.openHomeFragment();
            }
        });
    }

    private boolean isCodeValid() {
        if (binding.code.getText().toString().equalsIgnoreCase("123")) {
           return true;
        }
        return false;
    }

    private boolean isCodeEmpty() {
        String code = binding.code.getText().toString();
        if (TextUtils.isEmpty(code)) {
            return true;
        }
        return false;
    }
}